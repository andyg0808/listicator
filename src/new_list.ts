import { createAction } from "@reduxjs/toolkit";

export const newList = createAction<undefined>("newList");
